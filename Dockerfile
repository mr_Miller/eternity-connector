FROM 458034459534.dkr.ecr.eu-central-1.amazonaws.com/animal-id/microservice-base:php74 as dev

ARG COMPOSER_ALLOW_SUPERUSER=0
ENV COMPOSER_ALLOW_SUPERUSER=${COMPOSER_ALLOW_SUPERUSER}

COPY . /var/www/eternity-connector
WORKDIR /var/www/eternity-connector

RUN apk --update add --no-cache alpine-sdk build-base git && \
    composer install && \
    curl -L https://cs.symfony.com/download/php-cs-fixer-v3.phar -o /usr/local/bin/php-cs-fixer && \
    chmod +x /usr/local/bin/php-cs-fixer

EXPOSE 80 9000
CMD ["php-fpm", "--nodaemonize", "--fpm-config", "/etc/php/fpm.conf"]
