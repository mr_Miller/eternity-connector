<?php

namespace Eternity\Connector\Http\Connectors\Gateway;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Connector\Http\Connectors\Gateway\AnimalId\AnimalIdConnector;
use Eternity\Connector\Http\Connectors\Gateway\Location\LocationConnector;
use Eternity\Connector\Http\Connectors\Gateway\Users\Responses\UserResponse;
use Eternity\Definitions\HeadersDefinition;
use Eternity\Helpers\Uri;
use Eternity\Http\Client;

/**
 * Class GatewayConnector
 * @property \Eternity\Connector\Http\Connectors\Gateway\Location\LocationConnector $location
 * @property \Eternity\Connector\Http\Connectors\Gateway\AnimalId\AnimalIdConnector $animalId
 *
 * @package App\Application\Components\Connectors\Gateway
 */
class GatewayConnector extends AbstractConnector
{
    /**
     * GatewayConnector constructor.
     * @param \Eternity\Http\Client $client
     * @param string $serviceUrl
     * @param string $traceId
     * @param array $headers
     */
    public function __construct(Client $client, string $serviceUrl, string $traceId, array $headers = [])
    {
        parent::__construct($client, $serviceUrl, $traceId, $headers);

        $this->addSubConnector(LocationConnector::class, 'location');
        $this->addSubConnector(AnimalIdConnector::class, 'animalId');
    }

    /**
     * @param int $uid
     * @param int $requestedUid
     * @return \Eternity\Connector\Http\Connectors\Gateway\Users\Responses\UserResponse
     */
    public function getUser(int $uid, int $requestedUid): UserResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $requestedUid,
        ];

        $response = $this->get(Uri::build("v1/service/users/$uid"), 200, $headers);

        return new UserResponse($response);
    }
}
