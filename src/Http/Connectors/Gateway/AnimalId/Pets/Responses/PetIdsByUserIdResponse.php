<?php

namespace Eternity\Connector\Http\Connectors\Gateway\AnimalId\Pets\Responses;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Exceptions\EternityException;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class PetIdsByUserIdResponse
 * @package App\Application\Components\Connectors\Gateway\Location\Geolocation\Responses
 */
class PetIdsByUserIdResponse extends AbstractResponse
{
    /**
     * @var array
     */
    private $petIds;

    /**
     * PetsIdsByUserIdResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @throws \Eternity\Exceptions\EternityException
     */
    public function __construct(ExtendedResponse $response)
    {
        parent::__construct($response);
        if (!isset($this->getResponse()->getPayloadItem()['ids'])) {
            throw new EternityException(
                'Internal communication error',
                "Field 'ids' is missing in response from Location -> Gateway service"
            );
        }
        $this->petIds = $this->getResponse()->getPayloadItem()['ids'];
    }

    /**
     * @return array
     */
    public function petIds(): array
    {
        return $this->petIds;
    }
}
