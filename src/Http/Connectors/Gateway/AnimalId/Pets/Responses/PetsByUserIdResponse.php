<?php

namespace Eternity\Connector\Http\Connectors\Gateway\AnimalId\Pets\Responses;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Exceptions\EternityException;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class PetIdsByUserIdResponse
 * @package App\Application\Components\Connectors\Gateway\Location\Geolocation\Responses
 */
class PetsByUserIdResponse extends AbstractResponse
{
    /**
     * @var array
     */
    private $pets;

    /**
     * PetsByUserIdResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @throws \Eternity\Exceptions\EternityException
     */
    public function __construct(ExtendedResponse $response)
    {
        parent::__construct($response);
        if (empty($this->getResponse()->getPayloadItems())) {
            throw new EternityException(
                'Internal communication error',
                "Fields are missing in response from Location -> Gateway service"
            );
        }
        $this->pets = $this->getResponse()->getPayloadItems();
    }

    /**
     * @return array
     */
    public function pets(): array
    {
        return $this->pets;
    }
}
