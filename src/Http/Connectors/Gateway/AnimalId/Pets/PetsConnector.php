<?php

namespace Eternity\Connector\Http\Connectors\Gateway\AnimalId\Pets;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Connector\Http\Connectors\Gateway\AnimalId\Pets\Responses\PetIdsByUserIdResponse;
use Eternity\Connector\Http\Connectors\Gateway\AnimalId\Pets\Responses\PetsByUserIdResponse;
use Eternity\Definitions\HeadersDefinition;

/**
 * Class PetsConnector
 * @package App\Application\Components\Connectors\Gateway\AnimalId\Pets
 */
class PetsConnector extends AbstractConnector
{
    /**
     * Returns list of pets
     *
     * This method does not require Authentication
     *
     * @throws \Eternity\Exceptions\EternityException
     */
    public function getPetsByUserId(
        int $userId,
        string $orderBy = 'id',
        string $orderDirection = 'ASC',
        int $pageCurrent = 1,
        int $pageSize = 30
    ): PetsByUserIdResponse {
        $response = $this->get(
            "v1/service/users/$userId/pets",
            200,
            [
                HeadersDefinition::USER_ID => $userId,
                HeadersDefinition::ORDER_DIRECTION => $orderDirection,
                HeadersDefinition::ORDER_BY => $orderBy,
                HeadersDefinition::PAGE_CURRENT => $pageCurrent,
                HeadersDefinition::PAGE_SIZE => $pageSize,
            ]
        );

        return new PetsByUserIdResponse($response);
    }

    /**
     * Returns list of pet ID's
     *
     * This method does not require Authentication!
     *
     * Response structure:
     * {
     *   payload: [
     *     {
     *       ids: [1,2,3,4,5,6...]
     *     }
     *  ]
     * }
     *
     * @throws \Eternity\Exceptions\EternityException
     */
    public function getPetsIdsByUserId(int $userId): PetIdsByUserIdResponse
    {
        $response = $this->get(
            "v1/service/pets/ids",
            200,
            [
                HeadersDefinition::USER => $userId,
            ]
        );

        return new PetIdsByUserIdResponse($response);
    }
}
