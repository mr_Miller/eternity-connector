<?php

namespace Eternity\Connector\Http\Connectors\Gateway\AnimalId;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Connector\Http\Connectors\Gateway\AnimalId\Pets\PetsConnector;
use Eternity\Http\Client;

/**
 * Class AnimalIdConnector
 *
 * @property \Eternity\Connector\Http\Connectors\Gateway\AnimalId\Pets\PetsConnector $pets
 * @package App\Application\Components\Connectors\Gateway\AnimalId
 */
class AnimalIdConnector extends AbstractConnector
{
    /**
     * AnimalIdConnector constructor.
     * @param \Eternity\Http\Client $client
     * @param string $serviceUrl
     * @param string $traceId
     * @param array $headers
     */
    public function __construct(Client $client, string $serviceUrl, string $traceId, array $headers = [])
    {
        parent::__construct($client, $serviceUrl, $traceId, $headers);

        $this->addSubConnector(PetsConnector::class, 'pets');
    }
}
