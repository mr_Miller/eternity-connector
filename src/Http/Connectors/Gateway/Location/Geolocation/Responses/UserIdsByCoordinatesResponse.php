<?php

namespace Eternity\Connector\Http\Connectors\Gateway\Location\Geolocation\Responses;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Exceptions\EternityException;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class UserIdsByCoordinatesResponse
 * @package App\Application\Components\Connectors\Gateway\Location\Geolocation\Responses
 */
class UserIdsByCoordinatesResponse extends AbstractResponse
{
    /**
     * @var array
     */
    private $userIds;

    /**
     * UserIdsByCoordinatesResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @throws \Eternity\Exceptions\EternityException
     */
    public function __construct(ExtendedResponse $response)
    {
        parent::__construct($response);
        if (!isset($this->getResponse()->getPayloadItem()['ids'])) {
            throw new EternityException(
                'Internal communication error',
                "Field 'ids' is missing in response from Location -> Gateway service"
            );
        }
        $this->userIds = $this->getResponse()->getPayloadItem()['ids'];
    }

    /**
     * @return array
     */
    public function userIds(): array
    {
        return $this->userIds;
    }
}
