<?php

namespace Eternity\Connector\Http\Connectors\Gateway\Location\Geolocation;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Connector\Http\Connectors\Gateway\Location\Geolocation\Responses\UserIdsByCoordinatesResponse;

/**
 * Class GeolocationConnector
 * @package App\Application\Components\Connectors\Gateway\Location\Geolocation
 */
class GeolocationConnector extends AbstractConnector
{
    /**
     * Returns list of user ID's within distance to provided coordinates
     *
     * This method does not require Authentication!
     *
     * Response structure:
     * {
     *   payload: [
     *     {
     *       ids: [1,2,3,4,5,6...]
     *     }
     *  ]
     * }
     *
     * @param float $lat
     * @param float $lng
     * @param int $distance
     * @return \Eternity\Connector\Http\Connectors\Gateway\Location\Geolocation\Responses\UserIdsByCoordinatesResponse
     * @throws \Eternity\Exceptions\EternityException
     */
    public function getUserIdsByCoordinates(float $lat, float $lng, int $distance): UserIdsByCoordinatesResponse
    {
        $response = $this->get(
            "v1/service/location/users/coordinates/$lat/$lng/distance/$distance",
        );

        return new UserIdsByCoordinatesResponse($response);
    }
}
