<?php

namespace Eternity\Connector\Http\Connectors\Gateway\Location;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Connector\Http\Connectors\Gateway\Location\Geolocation\GeolocationConnector;
use Eternity\Http\Client;

/**
 * Class LocationConnector
 *
 * @property \Eternity\Connector\Http\Connectors\Gateway\Location\Geolocation\GeolocationConnector $geolocation
 * @package App\Application\Components\Connectors\Gateway\Location
 */
class LocationConnector extends AbstractConnector
{
    /**
     * LocationConnector constructor.
     * @param \Eternity\Http\Client $client
     * @param string $serviceUrl
     * @param string $traceId
     * @param array $headers
     */
    public function __construct(Client $client, string $serviceUrl, string $traceId, array $headers = [])
    {
        parent::__construct($client, $serviceUrl, $traceId, $headers);

        $this->addSubConnector(GeolocationConnector::class, 'geolocation');
    }
}
