<?php

namespace Eternity\Connector\Http\Connectors\Gateway\Users\Dto;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class UserDto
 * @package Eternity\Connector\Http\Connectors\Gateway\Users\Dto
 */
class UserDto implements Arrayable
{
    use FromArray;

    /** @var int */
    public $id;

    /** @var string|null */
    public $email;

    /** @var bool */
    public $is_email_verified;

    /** @var string|null */
    public $phone;

    /** @var bool */
    public $is_phone_verified;

    /** @var string|null */
    public $first_name;

    /** @var string|null */
    public $last_name;

    /** @var string|null */
    public $gender;

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'                => $this->id,
            'email'             => $this->email,
            'is_email_verified' => $this->is_email_verified,
            'phone'             => $this->phone,
            'is_phone_verified' => $this->is_phone_verified,
            'first_name'        => $this->first_name,
            'last_name'         => $this->last_name,
            'gender'            => $this->gender,
        ];
    }
}