<?php

namespace Eternity\Connector\Http\Connectors\Gateway\Users\Responses;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Connector\Http\Connectors\Gateway\Users\Dto\UserDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class UserResponse
 * @package Eternity\Connector\Http\Connectors\Gateway\Users\Responses
 */
class UserResponse extends AbstractResponse
{
    /** @var \Eternity\Connector\Http\Connectors\Gateway\Users\Dto\UserDto */
    protected $userDto;

    /**
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(ExtendedResponse $response)
    {
        parent::__construct($response);
        $this->userDto = UserDto::fromArray($response->getPayloadItem());
    }

    /**
     * @return \Eternity\Connector\Http\Connectors\Gateway\Users\Dto\UserDto
     */
    public function user(): UserDto
    {
        return $this->userDto;
    }
}